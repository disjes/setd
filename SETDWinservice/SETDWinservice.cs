﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MVCBase;
using SETDWinservice;
namespace SETDWinservice
{
    public partial class SETDWinservice : ServiceBase
    {
        
        Timer mTimer = new Timer();
        SETDEntities db = new SETDEntities();

        public SETDWinservice()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            mTimer.Interval = 120000;
            mTimer.Start();
            mTimer.Elapsed += MTimerOnElapsed;
        }

        private void MTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            db = new SETDEntities();
            var enviosToSend = db.Envios.Where(x => x.state == "I" || x.state == "E");
            foreach (var envio in enviosToSend) 
            {                                 
                var nominado = db.Nominados.FirstOrDefault(x => x.idNominado == envio.idNominado);                
                if (nominado != null)
                {
                    long ticks = DateTime.Now.Ticks;
                    string token = CreateHash(nominado.idNominado.ToString() + envio.idEncuesta +
                        "Bu3n4C14v31221" + ticks);
                    SendMail(nominado, envio, token, ticks);
                }                
            }
        }

        private void SendMail(Nominados nominado, Envios envio, string token, long ticks)
        {
            try
            {
                GetClient().Send(GetMessage(nominado, envio, token));
            }
            catch (Exception)
            {
                MSenderOnSendCompleted(envio.idEnvio, "E", null, 0);
                return;
            }
            MSenderOnSendCompleted(envio.idEnvio, "R", token, ticks);
        }

        private MailMessage GetMessage(Nominados nominado, Envios envio, string token)
        {           
            return new MailMessage(Helper.GetConf("Correo"),
                nominado.correo,
                Helper.GetConf("MailSubject"),
                Helper.GetConf("Domain") +
                "/Encuestas/Create?user=" + nominado.idNominado +
                "&idEncuesta=" + envio.idEncuesta +
                "&token=" + token);
        }

        private SmtpClient GetClient()
        {
            return new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Helper.GetConf("Correo"),
                    Helper.GetConf("ContraseniaCorreo"))
            };
        }

        private string CreateHash(string values)
        {
            byte[] encodedValues = new UTF8Encoding().GetBytes(values);
            byte[] hash = ((HashAlgorithm) CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedValues);
            return BitConverter.ToString(hash).Replace("-", String.Empty).ToLower();
        }

        private void MSenderOnSendCompleted(int idEnvio, string estado, string hash, long ticks)
        {
            SETDEntities dbState = new SETDEntities();
            var envio = dbState.Envios.FirstOrDefault(x => x.idEnvio == idEnvio);
            if (envio != null)
            {
                envio.state = estado;
                dbState.Entry(envio).State = EntityState.Modified;
                dbState.Auth.Add(new Auth() { idEncuesta = envio.idEncuesta, token = hash, idNominado = envio.idNominado, time = ticks });
                dbState.SaveChanges();                
            }
        }

        protected override void OnStop()
        {
        }

        public void OnDebug()            
        {
            OnStart(null);
        }

    }
}
