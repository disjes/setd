﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class PrioridadesController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Prioridades/
        public ActionResult Index()
        {
            return View(db.Prioridades.Where(x => x.activo == true).ToList());
        }

        // GET: /Prioridades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prioridades prioridades = db.Prioridades.Find(id);
            if (prioridades == null)
            {
                return HttpNotFound();
            }
            return View(prioridades);
        }

        // GET: /Prioridades/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Prioridades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idPrioridad,descripcionPrioridad")] Prioridades prioridades)
        {
            if (ModelState.IsValid)
            {
                prioridades.activo = true;
                db.Prioridades.Add(prioridades);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(prioridades);
        }

        // GET: /Prioridades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prioridades prioridades = db.Prioridades.Find(id);
            if (prioridades == null)
            {
                return HttpNotFound();
            }
            return View(prioridades);
        }

        // POST: /Prioridades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idPrioridad,descripcionPrioridad")] Prioridades prioridades)
        {
            if (ModelState.IsValid)
            {
                prioridades.activo = true;
                db.Entry(prioridades).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(prioridades);
        }

        // GET: /Prioridades/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prioridades prioridades = db.Prioridades.Find(id);
            if (prioridades == null)
            {
                return HttpNotFound();
            }
            return View(prioridades);
        }

        // POST: /Prioridades/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Prioridades prioridades = db.Prioridades.Find(id);
            prioridades.activo = false;
            db.Entry(prioridades).State = EntityState.Modified; 
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
