﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Data.Entity;
using WebGrease.Css.Extensions;

namespace MVCBase.Controllers
{
    public class StatisticsController : Controller
    {
        SETDEntities db = new SETDEntities();
        //
        // GET: /Statistics/
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.seccion = "Encuestas";
            ViewBag.view = "Listado de Encuestas";
            var encuestaMaster = db.EncuestaMaster;
            var prioridades = new List<Prioridades>();
            var nominados = new List<NominadoViewModel>();
            var encuesta = db.EncuestasMaster.FirstOrDefault(x => x.idEncuesta == 1);
            if (encuesta != null)
                encuesta.EncuestasDetalle.Where(x => x.activo == true).ForEach(item =>
                {                    
                    prioridades.Add(db.Prioridades.FirstOrDefault(x => x.idPrioridad == item.idPrioridad && item.Prioridades.activo == true));
                });
            ViewBag.prioridades = prioridades.OrderBy(x => x.orden).ToList();
            return View(encuestaMaster.Where(x => x.activo == true).ToList());
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "IdEncuesta,IdNominado,Check, IdEncuestaRespuesta")] EncuestaViewModel[] encuestas)
        {
            var mIdsEncuestas = encuestas.Where(x => x.Check).Select(x => x.IdEncuesta);
            if (mIdsEncuestas.ToList().Count > 0)
            {
                var mIdsEncuestasRespuestas = encuestas.Where(x => x.Check).Select(x => x.IdEncuestaRespuesta);
                var mEncuestas = db.EncuestaMaster.Where(x => mIdsEncuestasRespuestas.Contains(x.idEncuestaMaster)).ToList();
                int numComponentes = mEncuestas.ToArray()[0].EncuestaDetalle.Count;
                var componentes = db.Conceptos;            
                var mComponentes = new List<Conceptos>();
                mEncuestas.ToList()[0].EncuestaDetalle.ForEach((item) =>
                {
                    mComponentes.Add(componentes.FirstOrDefault(x => x.idConcepto == item.Conceptos.idConcepto));
                });
                List<int> sum = GetSum(mComponentes, mIdsEncuestasRespuestas.ToList());
                return Kendall(numComponentes, mIdsEncuestas, mIdsEncuestasRespuestas, sum); 
            }
            ViewBag.message = "Debe seleccionar al menos un nominado para que sea evaluado.";
            ViewBag.enlace = "/Statistics/";
            return View("MessageView");
        }

        private List<int> GetSum(List<Conceptos> mComponentes, List<int> SelectedEncuestas)
        {
            var sum = new List<int>();
            foreach (var componente in mComponentes)
            {
                var mComponente = componente;
                var resultados = from encuestadetalle in db.EncuestaDetalle
                                 where encuestadetalle.idConcepto == mComponente.idConcepto
                                 select encuestadetalle;
                resultados = resultados.Where(x => SelectedEncuestas.Contains((int)x.idEncuestaMaster));
                sum.Add(Convert.ToInt32(resultados.Select(x => x.respuesta).Sum()));
            }
            return sum;
        }

        private ActionResult Kendall(int numComponentes, IEnumerable<int> mIdsEncuestas, IEnumerable<int> mIdsEncuestasRespuestas, List<int> sum)
        {
            int k = numComponentes;
            int m = mIdsEncuestas.ToList().Count;
            Application app = new Application();
            double dvsq = app.WorksheetFunction.DevSq(sum.ToArray());
            double w = (12 * dvsq) / ((m * m) * ((k * k * k) - k));
            double r = (k * m - 1) / (m - 1);
            double xsquare = m * (k - 1) * w;
            int df = k - 1;
            double p = app.WorksheetFunction.ChiDist(xsquare, df);
            List<CalculosKendallDetail> details = new List<CalculosKendallDetail>();
            foreach (var enc in mIdsEncuestasRespuestas) details.Add(new CalculosKendallDetail() { idEncuesta = enc });
            AddKendall(numComponentes, m, dvsq, w, r, xsquare, df, p, details);            
            db.SaveChanges();
            return RedirectToAction("Index", "Kendall");
        }

        private void AddKendall(int numComponentes, int m, double dvsq, double w, double r, double xsquare, int df, double p, 
            List<CalculosKendallDetail> details)
        {
            db.CalculosKendall.Add(new CalculosKendall()
            {
                k = numComponentes,
                m = m,
                dvsq = Convert.ToDecimal(dvsq),
                w = Convert.ToDecimal(w),
                r = Convert.ToDecimal(r),
                xsquare = Convert.ToDecimal(xsquare),
                df = df,
                p = Convert.ToDecimal(p),
                estado = "A",
                fecha = DateTime.Now,
                CalculosKendallDetail = details,
                usuario = User.Identity.Name
            });
        }

        public ActionResult EndKendall()
        {
            var kendalls = db.CalculosKendall.Where(x => x.usuario == User.Identity.Name && x.estado == "A");
            kendalls.ForEach((item) =>
            {
                item.estado = "D";
            });
            foreach (var kendall in kendalls) db.Entry(kendall).State = EntityState.Modified;
            db.SaveChanges();
            return View();
        }

        //
        // GET: /Statistics/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Statistics/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Statistics/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Statistics/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Statistics/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Statistics/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Statistics/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
