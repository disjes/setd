﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class KendallController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Kendall/
        public ActionResult Index()
        {
            ViewBag.History = false;
            var calculoskendall = db.CalculosKendall.Where(x => x.estado == "A");
            return View(calculoskendall.ToList());
        }

        public ActionResult Historial()
        {
            ViewBag.History = true;
            var calculoskendall = db.CalculosKendall.Where(x => x.estado == "D");
            return View("Index", calculoskendall.ToList());
        }

        public ActionResult EndKendall()
        {
            var kendalls = db.CalculosKendall.Where(x => x.usuario == User.Identity.Name && x.estado == "A");
            foreach (var kendall in kendalls)
            {
                kendall.estado = "D";                
                db.Entry(kendall).State = EntityState.Modified;
            }
            decimal pLimit = decimal.Parse("0.5");
            var maxw = db.CalculosKendall.Where(x => x.p < pLimit && x.estado == "A" && x.usuario == User.Identity.Name);
            if(maxw.ToList().Count > 0) 
            {
                var judges = maxw.OrderByDescending(x => x.w).First();            
                foreach (var judge in judges.CalculosKendallDetail)
                    db.Envios.Add(new Envios()
                    {
                        fecha = DateTime.Now,
                        idEncuesta = 2,
                        idNominado = judge.EncuestaMaster.Nominados.idNominado,
                        state = "I"
                    });
            }
            db.SaveChanges();            
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
