﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;
using System.IO;
using Microsoft.Office.Interop.Excel;
using WebGrease.Css.Extensions;

namespace MVCBase.Controllers
{
    public class EncuestasController : Controller
    {
        private SETDEntities db = new SETDEntities();

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.seccion = "Encuestas";
            ViewBag.view = "Listado de Encuestas";
            var encuestaMaster = db.EncuestaMaster;
            var prioridades = new List<Prioridades>();
            var nominados = new List<NominadoViewModel>();
            var encuesta = db.EncuestasMaster.FirstOrDefault(x => x.idEncuesta == 1);
            if (encuesta != null)
                encuesta.EncuestasDetalle.ToList().ForEach((item) =>
                {
                    prioridades.Add(db.Prioridades.FirstOrDefault(x => x.idPrioridad == item.idPrioridad && item.Prioridades.activo == true));                    
                });
            ViewBag.prioridades = prioridades.OrderBy(x => x.orden).ToList();
            return View(encuestaMaster.Where(x => x.activo == true).ToList());
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "IdEncuesta,IdNominado,Check")] EncuestaViewModel[] encuestas)
        {
            var mIdsEncuestas = encuestas.Where(x => x.Check).Select(x => x.IdEncuesta);
            var mEncuestas = db.EncuestaMaster.Where(x => mIdsEncuestas.Contains(x.idEncuesta));
            int numComponentes = mEncuestas.ToArray()[0].EncuestaDetalle.Count;
            var componentes = db.Conceptos;
            var mComponentes = new List<Conceptos>();
            mEncuestas.ToList()[0].EncuestaDetalle.ForEach((item) =>
            {
                mComponentes.Add(componentes.FirstOrDefault(x => x.idConcepto == item.Conceptos.idConcepto));
            });
            List<int> sum = new List<int>();
            foreach (var componente in mComponentes)
            {
                var mComponente = componente;
                var resultados = from encuestadetalle in db.EncuestaDetalle
                            where encuestadetalle.idConcepto == mComponente.idConcepto
                            select encuestadetalle;
                sum.Add(Convert.ToInt32(resultados.Select(x => x.respuesta).Sum()));
            }
            int k = numComponentes;
            int m = mIdsEncuestas.ToList().Count;
            Application app = new Application();
            double dvsq = app.WorksheetFunction.DevSq(sum.ToArray());
            double w = (12 * dvsq) / (( m * m) * (( k * k * k) - k));
            double r = (k * m - 1) / (k - 1);
            double xsquare = m * (k - 1) * w;
            double df = k - 1;
            double p = app.WorksheetFunction.ChiDist(xsquare, df);
            ViewBag.w = w;
            ViewBag.p = p;
            return View("Statistics");
        }

        // GET: /Default2/Create
        [AllowAnonymous]
        public ActionResult Create(int user, string idEncuesta, string token)
        {
            ViewBag.Preview = false;
            if (ValidateStuff(idEncuesta, user, token))
            {                
                SetupEncuesta(idEncuesta);
                ViewBag.seccion = "Encuestas";
                ViewBag.view = "Crear Encuesta";                
                return View(SetupEncuestaMaster(idEncuesta, user, ViewBag.conceptos as List<Conceptos>));   
            }
            return RedirectToAction("Message", "Helper", new { message = "El enlace a la encuesta expiro o no existe!", enlace = "", showEnlace = false });
        }

        private bool ValidateStuff(string idEncuesta, int user, string token)
        {
            if (token == "preview") { ViewBag.Preview = true; return true;}
            if (idEncuesta == "25") CreateRepeat(user, idEncuesta, token);
            int idEncuestaInt = Convert.ToInt32(idEncuesta);
            var encuestaExistente = db.EncuestaMaster.FirstOrDefault(x => x.Nominados.idNominado == user && x.idEncuesta == idEncuestaInt);
            if (encuestaExistente == null || encuestaExistente.fecha == null || 
                encuestaExistente.fecha.Value.AddMonths(2) > DateTime.Now) return false;            
            return ValidateToken(token, user);
        }

        private bool ValidateToken(string token, int user)
        {            
            var auth = db.Auth.FirstOrDefault(x => x.token == token);
            if (auth != null)
            {
                if (auth.time != null)
                {
                    var timeLimit = new DateTime((long)auth.time).AddDays(Convert.ToInt32(Helper.GetConf("DiasEspera")));
                    ViewBag.usuario = user;
                    return DateTime.Now < timeLimit;
                }
                return false;
            }
            return false;
        }

        private void SetupEncuesta(string idEncuesta)
        {
            List<Prioridades> prioridades = SetupPrioridades(idEncuesta);            
            List<Conceptos> conceptos = SetUpComponentes(prioridades);
            ViewBag.prioridades = prioridades.OrderBy(x => x.orden).ToList();
            ViewBag.conceptos = conceptos.OrderBy(x => x.orden).ToList();           
        }

        private EncuestaMaster SetupEncuestaMaster(string idEncuesta, int user, List<Conceptos> conceptos )
        {
            return new EncuestaMaster
            {
                idEncuesta = Convert.ToInt32(idEncuesta),
                fecha = DateTime.Now,
                usuario = user,
                EncuestaDetalle = CreateRespuestas(conceptos.OrderBy(x => x.orden).ToList())
            };
        }

        private List<Prioridades> SetupPrioridades(string idEncuesta)
        {
            List<Prioridades> prioridades = new List<Prioridades>();
            int idEncuestaInt = Convert.ToInt32(idEncuesta);
            var firstOrDefault = db.EncuestasMaster.FirstOrDefault(x => x.idEncuesta == idEncuestaInt);
            if (firstOrDefault != null)
                firstOrDefault.EncuestasDetalle.ToList().ForEach((item) =>
                {
                    if (item.activo == true && item.Prioridades.activo == true) prioridades.Add(db.Prioridades.FirstOrDefault(x => x.idPrioridad == item.idPrioridad));
                });
            return prioridades;
        }

        private List<Conceptos> SetUpComponentes(List<Prioridades> prioridades)
        {
            List<Conceptos> conceptos = new List<Conceptos>();
            prioridades.ForEach((item) =>
            {
                conceptos.AddRange(item.Conceptos.Where(x => x.activo == true));
            });
            return conceptos;
        }

        public List<EncuestaDetalle> CreateRespuestas(List<Conceptos> mConceptos)
        {
            List<EncuestaDetalle> mEncuestasDetalle = new List<EncuestaDetalle>();
            foreach (Conceptos mConcepto in mConceptos)
            {
                EncuestaDetalle mDetalle = new EncuestaDetalle();
                mDetalle.idConcepto = mConcepto.idConcepto;
                mDetalle.idPrioridad = mConcepto.idPrioridad;                
                mEncuestasDetalle.Add(mDetalle);
            }
            return mEncuestasDetalle;
        }

        [AllowAnonymous]
        private ActionResult CreateRepeat(int user, string idRepeat, string token)
        {
            ViewBag.seccion = "Encuestas";
            ViewBag.view = "Crear Encuesta";
            var auth = db.Auth.FirstOrDefault(x => x.token == token);
            if (auth != null)
            {
                if (auth.time != null)
                {
                    var time = new DateTime((long)auth.time).AddDays(3);
                    ViewBag.usuario = user;
                    if (DateTime.Now > time) return HttpNotFound("La encuesta ya no esta disponible");
                }
            }
            List<Prioridades> prioridades = new List<Prioridades>();
            int idRepeatInt = Convert.ToInt32(idRepeat);
            db.Repeats.Where(x => x.idNominado == user).ForEach((item) =>
            {
                var prioridad = db.EncuestaDetalle.FirstOrDefault(x => x.idPrioridad == item.EncuestaDetalle.idPrioridad);
            });
            
            var firstOrDefault = db.EncuestasMaster.FirstOrDefault(x => x.idEncuesta == idRepeatInt);
            if (firstOrDefault != null)
                firstOrDefault.EncuestasDetalle.ForEach((item) =>
                {
                    if (item.activo == true && item.Prioridades.activo == true) prioridades.Add(db.Prioridades.FirstOrDefault(x => x.idPrioridad == item.idPrioridad));
                });
            ViewBag.prioridades = prioridades.OrderBy(x => x.orden).ToList();
            List<Conceptos> conceptos = new List<Conceptos>();
            prioridades.ForEach((item) =>
            {
                conceptos.AddRange(item.Conceptos.Where(x => x.activo == true));
            });
            ViewBag.conceptos = conceptos.OrderBy(x => x.orden).ToList();
            var encuestaMaster = new EncuestaMaster
            {
                idEncuesta = Convert.ToInt32(idRepeat),
                fecha = DateTime.Now,
                usuario = user,
                EncuestaDetalle = CreateRespuestas(conceptos.OrderBy(x => x.orden).ToList())
            };
            return View(encuestaMaster);
        }

        // POST: /Default2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idEncuesta,usuario,EncuestaDetalle")] EncuestaMaster EncuestaMaster)
        {            
            if (ModelState.IsValid)
            {                
                EncuestaMaster.activo = true;
                EncuestaMaster.fecha = DateTime.Now;
                db.EncuestaMaster.Add(EncuestaMaster);
                db.SaveChanges();                         
                return View("EncuestaSuccess");
            }
            //HERE BUQUES
            return HttpNotFound("Ocurrio un error al llenar la encuesta intentelo de nuevo o pongase en contacto con el administrador del sistema.");
        }

        // POST: /Default2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRepeat([Bind(Include = "idEncuesta,usuario,EncuestaDetalle")] EncuestaMaster EncuestaMaster)
        {
            if (ModelState.IsValid)
            {
                foreach (var detalle in EncuestaMaster.EncuestaDetalle)
                {
                    EncuestaMaster.activo = true;
                    EncuestaMaster.fecha = DateTime.Now;
                    db.Entry(detalle).State = EntityState.Modified;
                }                                
                db.SaveChanges();
                doSendRepeatsAndAllKappaStuff(EncuestaMaster);
                return View("EncuestaSuccess");
            }

            //HERE BUQUES
            return HttpNotFound("Ocurrio un error al llenar la encuesta intentelo de nuevo o pongase en contacto con el administrador del sistema.");
        }

        private void doSendRepeatsAndAllKappaStuff(EncuestaMaster EncuestaMaster)
        {
            var mKappas = Kappa.KappaOk(User.Identity.Name);
            db.Repeats.RemoveRange(db.Repeats.Where(x => x.idNominado == EncuestaMaster.Nominados.idNominado));
            var mKappaForCount = mKappas.FirstOrDefault(x => x.Si > 0 && x.No > 0);
            var maxResend = Convert.ToInt32(db.Configs.FirstOrDefault(y => y.config == "maxResend").valor);
            var currentResendSent = Convert.ToInt32(db.Configs.FirstOrDefault(y => y.config == "currentResendSent").valor);
            var loEnvia = maxResend > currentResendSent;
            if (!loEnvia) endKappa();
            if (db.Repeats.FirstOrDefault(x => x.EncuestaDetalle.EncuestaMaster.idEncuesta == 2) == null && loEnvia) 
                Kappa.resendSurvey(mKappaForCount.Si + mKappaForCount.No, mKappas);
        }

        private void endKappa()
        {
            
        }

        // GET: /Default2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestaMaster encuestaMaster = db.EncuestaMaster.Find(id);
            if (encuestaMaster == null)
            {
                return HttpNotFound();
            }
            return View(encuestaMaster);
        }

        // POST: /Default2/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            EncuestaMaster encuestaMaster = db.EncuestaMaster.Find(id);
            encuestaMaster.activo = false;
            db.Entry(encuestaMaster).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
