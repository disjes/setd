﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCBase.Controllers
{
    public class HelperController : Controller
    {
        //
        // GET: /Helper/
        public ActionResult Message(string message, string enlace, bool showEnlace)
        {
            ViewBag.message = message;
            ViewBag.enlace = enlace;
            ViewBag.showEnlace = showEnlace;
            return View("MessageView");
        }
    }
}
