﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class ComponentesController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Conceptos/
        public ActionResult Index()
        {
            var conceptos = db.Conceptos.Include(c => c.Prioridades);
            return View(conceptos.Where(x => x.activo == true).ToList());
        }

        // GET: /Conceptos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conceptos conceptos = db.Conceptos.Find(id);
            if (conceptos == null)
            {
                return HttpNotFound();
            }
            return View(conceptos);
        }

        // GET: /Conceptos/Create
        public ActionResult Create()
        {
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad");
            return View();
        }

        // POST: /Conceptos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idConcepto,idPrioridad,descripcionConcepto")] Conceptos conceptos)
        {
            if (ModelState.IsValid)
            {
                conceptos.activo = true;
                db.Conceptos.Add(conceptos);                
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", conceptos.idPrioridad);
            return View(conceptos);
        }

        // GET: /Conceptos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conceptos conceptos = db.Conceptos.Find(id);
            if (conceptos == null)
            {
                return HttpNotFound();
            }
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", conceptos.idPrioridad);
            return View(conceptos);
        }

        // POST: /Conceptos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idConcepto,idPrioridad,descripcionConcepto")] Conceptos conceptos)
        {
            if (ModelState.IsValid)
            {
                conceptos.activo = true;
                db.Entry(conceptos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", conceptos.idPrioridad);
            return View(conceptos);
        }

        // GET: /Conceptos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conceptos conceptos = db.Conceptos.Find(id);
            if (conceptos == null)
            {
                return HttpNotFound();
            }
            return View(conceptos);
        }

        // POST: /Conceptos/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Conceptos conceptos = db.Conceptos.Find(id);
            conceptos.activo = false;
            db.Entry(conceptos).State = EntityState.Modified;            
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
