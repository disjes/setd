﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class EmpresasController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Empresas/
        public ActionResult Index()
        {
            return View(db.Empresas.Where(x => x.activo == true).ToList());
        }

        // GET: /Empresas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        // GET: /Empresas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Empresas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idEmpresa,descripcionEmpresa")] Empresas empresas)
        {
            if (ModelState.IsValid)
            {
                empresas.activo = true;
                empresas.descripcionEmpresa = empresas.descripcionEmpresa.Trim();
                db.Empresas.Add(empresas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empresas);
        }

        // GET: /Empresas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        // POST: /Empresas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idEmpresa,descripcionEmpresa")] Empresas empresas)
        {
            if (ModelState.IsValid)
            {
                empresas.activo = true;
                empresas.descripcionEmpresa = empresas.descripcionEmpresa.Trim();
                db.Entry(empresas).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    
                    throw;
                }
                
                return RedirectToAction("Index");
            }
            return View(empresas);
        }

        // GET: /Empresas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        // POST: /Empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed([Bind(Include = "id")]int id)
        {
            Empresas empresas = db.Empresas.Find(id);
            empresas.activo = false;
            db.Entry(empresas).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
