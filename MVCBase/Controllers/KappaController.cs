using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.Office.Interop.Excel;
using MVCBase;
using WebGrease.Css.Extensions;

namespace MVCBase.Controllers
{
    public class KappaController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Kappa/
        public ActionResult Index()
        {
            var pendingSecondSurveyNotAnswered = db.Envios.Where(x => x.idEncuesta == 2 && x.state == "A");
            if (pendingSecondSurveyNotAnswered.ToList().Count == 0)
            {
                Kappa.KappaOk(User.Identity.Name);
                return View("Index", db.CalculosKappa);
            }
            return RedirectToAction("Message", "Helper", new { message = "La encuesta no ha sido respondida por todos los jueces!", enlace = "/Home/", showEnlace = true });
        }

        // GET: /Kappa/
        
        public ActionResult Listado()
        {            
            return View("Index", db.CalculosKappa);
        }

        public ActionResult KappaOk()
        {
            var encuestasRecords = db.EncuestaMaster.Where(x => x.idEncuesta == 2 && x.estado == "A").ToList();
            Application app = new Application();                      
            List<KappaModel> mKappaModels = new List<KappaModel>();            
            db.EncuestasDetalle.Where(x => x.EncuestasMaster.idEncuesta == 2).Select(x => x.Prioridades).ForEach(item =>
            {
                foreach (var concepto in item.Conceptos)
                {
                    mKappaModels.Add(new KappaModel() 
                    { 
                        IdPregunta = concepto.idConcepto, 
                        No = db.EncuestaDetalle.Where(x => x.EncuestaMaster.idEncuesta == 2 &&
                            x.idConcepto == concepto.idConcepto && x.respuesta == 2 && x.EncuestaMaster.estado == "A").ToList().Count,
                        Si = db.EncuestaDetalle.Where(x => x.EncuestaMaster.idEncuesta == 2 &&
                            x.idConcepto == concepto.idConcepto && x.respuesta == 1 && x.EncuestaMaster.estado == "A").ToList().Count,
                    });    
                }                
            });
            var mKappaForCount = mKappaModels.FirstOrDefault(x => x.Si > 0 && x.No > 0);
            CalculosKappa mKappa = new CalculosKappa();
            mKappa.m = mKappaForCount.Si + mKappaForCount.No;      
            mKappa.n = mKappaModels.Sum(x => x.Si);
            mKappa.qyes = mKappaModels.Sum(x => x.Si) / (mKappa.m * mKappa.n);
            mKappa.qno = mKappaModels.Sum(x => x.No);
            mKappa.byes = mKappa.qyes*(1 - mKappa.qyes);
            mKappa.bno = mKappa.qno*(1 - mKappa.qno);
            mKappa.kyes = 1 - Convert.ToDecimal(app.WorksheetFunction.SumProduct(mKappaModels.Select(x => x.Si), 8 - app.WorksheetFunction.Sum(mKappaModels.Select(x => x.Si)))) /
                          (mKappa.m*mKappa.n*mKappa.m - 1)*mKappa.qyes*(1 - mKappa.kyes); //Pendiente
            mKappa.kno = 1 - Convert.ToDecimal(app.WorksheetFunction.SumProduct(mKappaModels.Select(x => x.No), mKappaModels.Select(x => x.No))) /
                          (mKappa.m * mKappa.n * mKappa.m - 1) * mKappa.qno * (1 - mKappa.kno); //Pendiente
            mKappa.seyes = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(2 / (mKappa.m * (mKappa.m - 1) * mKappa.n))));
            mKappa.seno = mKappa.seyes;
            mKappa.zyes = mKappa.kyes/mKappa.seyes;
            mKappa.zno = mKappa.kno/mKappa.seyes;
            mKappa.pyes = Convert.ToDecimal(2*(1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.zyes), true)));
            mKappa.pno = Convert.ToDecimal(2*(1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.zno), true)));
            mKappa.Pa = (Convert.ToDecimal(app.WorksheetFunction.SumSq(mKappaModels.Select(x => x.Si),
                mKappaModels.Select(x => x.No))) - (mKappa.m * mKappa.n)) / (mKappa.m * mKappa.n * (mKappa.m - 1));
            mKappa.Pe = Convert.ToDecimal(app.WorksheetFunction.SumSq(mKappa.qyes, mKappa.qno));
            mKappa.Kappa = (mKappa.m * mKappa.n) / (1 - mKappa.n);
            double sumbexp2 = app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno) *
                              app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno);
            mKappa.se = Convert.ToDecimal(Convert.ToDouble(mKappa.seyes) * Math.Sqrt(Convert.ToDouble(sumbexp2 - app.WorksheetFunction.SumProduct(mKappa.byes,
                mKappa.bno, mKappa.qyes, mKappa.qno)))) / Convert.ToDecimal(app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno));
            mKappa.z = mKappa.Kappa/mKappa.se;
            mKappa.pvalue = Convert.ToDecimal(2 * (1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.z), true)));
            mKappa.alpha = Convert.ToDecimal(0.005);
            mKappa.lower = (mKappa.Kappa + mKappa.se) * Convert.ToDecimal(app.WorksheetFunction.Norm_S_Inv(Convert.ToDouble(mKappa.alpha / 2)));
            mKappa.upper = (mKappa.Kappa - mKappa.se) * Convert.ToDecimal(app.WorksheetFunction.Norm_S_Inv(Convert.ToDouble(mKappa.alpha / 2)));
            mKappa.usuario = User.Identity.Name;
            mKappa.estado = "A";
            db.CalculosKappa.Add(mKappa);
            db.SaveChanges();
            //if(mKappa.Kappa < Convert.ToDecimal(0.6)) resendSurvey(Convert.ToInt32(mKappa.m), mKappaModels);
            return View("Index", db.CalculosKappa);
        }

        private void resendSurvey(int numJudges, List<KappaModel> mKappas)
        {
            int percent50 = Convert.ToInt32(numJudges*0.50);
            var targets = mKappas.Where(x => x.No <= (mKappas.Count - percent50) && x.No > 0);
            foreach (var target in targets)
            {                
                Repeats mRepeat = new Repeats();
                var mEncuestaDetalle = db.EncuestaDetalle.FirstOrDefault(x => x.idEncuestaDetalle == target.IdPregunta);
                mRepeat.idNominado = mEncuestaDetalle.EncuestaMaster.Nominados.idNominado;
                mRepeat.idEncuestaDetalle = mEncuestaDetalle.idEncuestaDetalle;
                db.Repeats.Add(mRepeat);
                db.Envios.Add(new Envios()
                {
                    idEncuesta = 25,
                    idNominado = mRepeat.idNominado,
                    fecha = DateTime.Now,
                    state = "A"
                });
            }         
            db.SaveChanges();
        }

        // GET: /Kappa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Kappa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCalculoKappa,m,n,Pa,Pe,Kappa,se,z,pvalue,alpha,lower,upper," +
                                                 "qyes,qno,byes,bno,kyes,kno,seyes,seno,zyes,zno,pyes,pno,fecha," +
                                                 "estado,usuario")] CalculosKappa calculoskappa)
        {
            if (ModelState.IsValid)
            {
                db.CalculosKappa.Add(calculoskappa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calculoskappa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}