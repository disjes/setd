﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class EncuestasDefinitionController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /EncuestasDefinition/
        public ActionResult Index()
        {
            return View(db.EncuestasMaster.Where(x => x.activo == true).ToList());
        }

        // GET: /EncuestasDefinition/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //EncuestasMaster encuestasmaster = db.EncuestasMaster.Find(id);
            //if (encuestasmaster == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(encuestasmaster);            
            ViewBag.Preview = true;
            var nominado = db.Nominados.First();
            return RedirectToAction("Create", "Encuestas", new { user = nominado.idNominado, idEncuesta = id, token = "preview" });
        }

        // GET: /EncuestasDefinition/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /EncuestasDefinition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idEncuesta,descripcionEncuesta")] EncuestasMaster encuestasmaster)
        {
            if (ModelState.IsValid)
            {
                encuestasmaster.activo = true;
                db.EncuestasMaster.Add(encuestasmaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(encuestasmaster);
        }

        // GET: /EncuestasDefinition/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestasMaster encuestasmaster = db.EncuestasMaster.Find(id);
            if (encuestasmaster == null)
            {
                return HttpNotFound();
            }
            return View(encuestasmaster);
        }

        // POST: /EncuestasDefinition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idEncuesta,descripcionEncuesta")] EncuestasMaster encuestasmaster)
        {
            if (ModelState.IsValid)
            {
                encuestasmaster.activo = true;
                db.Entry(encuestasmaster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(encuestasmaster);
        }

        // GET: /EncuestasDefinition/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestasMaster encuestasmaster = db.EncuestasMaster.Find(id);
            if (encuestasmaster == null)
            {
                return HttpNotFound();
            }
            return View(encuestasmaster);
        }

        // POST: /EncuestasDefinition/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            EncuestasMaster encuestasmaster = db.EncuestasMaster.Find(id);
            encuestasmaster.activo = false;
            db.Entry(encuestasmaster).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
