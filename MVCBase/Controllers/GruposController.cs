﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;
using EntityState = System.Data.Entity.EntityState;

namespace MVCBase.Controllers
{
    public class GruposController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Grupos/
        public ActionResult Index()
        {
            return View(db.Grupos.Where(x => x.activo == true).ToList());
        }

        // GET: /Grupos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);
            if (grupos == null)
            {
                return HttpNotFound();
            }
            return View(grupos);
        }

        // GET: /Grupos/Create
        public ActionResult Create()
        {
            ViewBag.empresas = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa");
            ViewBag.puestos = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto");
            return View();
        }

        public ActionResult PartialNominados(string empresas, string puestos, int anios)//List<Grupos> mList)
        {
            if (empresas == "" || puestos == "" || anios == 0) return Content("");
            var nominadosVmForPartialView = new List<NominadoViewModel>();
            empresas = empresas.Substring(0, empresas.Length - 1);
            puestos = puestos.Substring(0, puestos.Length - 1);
            var table = new DataTable();
            try
            {                
                var query = "Select * from nominadosDetalle where idEmpresa in ( " + empresas + ") AND idPuesto in (" + puestos + ") AND anios = " + anios + " AND activo = '1'";
                using (SqlConnection connection = new SqlConnection("Data Source = DAVIDPC; initial catalog = SETD; integrated security = True; MultipleActiveResultSets = True;"))                
                //using (SqlConnection connection = new SqlConnection("Data Source=192.168.95.11;Initial Catalog=SETD;Integrated Security=False;User ID=david;Password=david123;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False"))
                {
                    connection.Open();
                    table.Load(new SqlCommand(query, connection).ExecuteReader());
                    connection.Close();
                }    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                if(table.Rows.Count > 0)
                    foreach (DataRow nominadoDetalle in table.Rows)
                    {
                        var id = Convert.ToInt32(nominadoDetalle["idNominado"]);
                        var idDetalle = Convert.ToInt32(nominadoDetalle["idNominadosDetalle"]);
                        var nominado = db.Nominados.FirstOrDefault(x => x.idNominado == id);
                        var nomiDetalle = db.NominadosDetalle.FirstOrDefault(x => x.idNominadosDetalle == idDetalle);
                        if (nominado != null && nomiDetalle != null && nominado.interesado)
                            nominadosVmForPartialView.Add(new NominadoViewModel() { IdNominado = nominado.idNominado, Check = false, Nombre = nominado.nombreNominado, Empresa = nomiDetalle.Empresas.descripcionEmpresa, Puesto = nomiDetalle.Puestos.descripcionPuesto, Years = (nomiDetalle.anios ?? 0) });                                                                          
                    }
            }
            catch (Exception ex)
            {
                return Content("");
            }
            if (table.Rows.Count == 0) return Content("");
            return PartialView("_NominadosGrupos", nominadosVmForPartialView);
        }
        
        // POST: /Grupos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IdNominado,Check")] NominadoViewModel[] nominados)
        {
            if (ModelState.IsValid)
            {
                var selectedNominados = nominados.Where(x => x.Check);
                foreach (var nominado in selectedNominados)
                {
                    db.Envios.Add(new Envios()
                    {
                        fecha = DateTime.Now,
                        idEncuesta = 1,
                        idNominado = nominado.IdNominado,
                        state = "I"
                    });
                }
                db.SaveChanges();
                //grupos.activo = true;
                //db.Grupos.Add(grupos);
                //db.SaveChanges();
                //return RedirectToAction("Index");
            }

            return RedirectToAction("Index", "Home");
        }

        // GET: /Grupos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);
            if (grupos == null)
            {
                return HttpNotFound();
            }
            return View(grupos);
        }

        // POST: /Grupos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idGrupo,campoQuery,valueCampoQuery")] Grupos grupos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grupos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grupos);
        }

        // GET: /Grupos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupos grupos = db.Grupos.Find(id);
            if (grupos == null)
            {
                return HttpNotFound();
            }
            return View(grupos);
        }

        // POST: /Grupos/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Grupos grupos = db.Grupos.Find(id);
            grupos.activo = false;
            db.Entry(grupos).State = EntityState.Modified; 
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
