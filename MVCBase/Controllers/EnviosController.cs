﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class EnviosController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Envios/
        public ActionResult Index()
        {
            var envios = db.Envios.Include(e => e.EncuestasMaster).Include(e => e.Nominados);
            return View(envios.ToList());
        }

        // GET: /Envios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Envios envios = db.Envios.Find(id);
            if (envios == null)
            {
                return HttpNotFound();
            }
            return View(envios);
        }

        // GET: /Envios/Create
        public ActionResult Create()
        {
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster, "idEncuesta", "descripcionEncuesta");
            ViewBag.idNominado = new SelectList(db.Nominados, "idNominado", "nombreNominado");
            return View();
        }

        // POST: /Envios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idEnvio,fecha,idNominado,idEncuesta,state")] Envios envios)
        {
            if (ModelState.IsValid)
            {
                db.Envios.Add(envios);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster, "idEncuesta", "descripcionEncuesta", envios.idEncuesta);
            ViewBag.idNominado = new SelectList(db.Nominados, "idNominado", "nombreNominado", envios.idNominado);
            return View(envios);
        }

        // GET: /Envios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Envios envios = db.Envios.Find(id);
            if (envios == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster, "idEncuesta", "descripcionEncuesta", envios.idEncuesta);
            ViewBag.idNominado = new SelectList(db.Nominados, "idNominado", "nombreNominado", envios.idNominado);
            return View(envios);
        }

        // POST: /Envios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idEnvio,fecha,idNominado,idEncuesta,state")] Envios envios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(envios).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster, "idEncuesta", "descripcionEncuesta", envios.idEncuesta);
            ViewBag.idNominado = new SelectList(db.Nominados, "idNominado", "nombreNominado", envios.idNominado);
            return View(envios);
        }

        // GET: /Envios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Envios envios = db.Envios.Find(id);
            if (envios == null)
            {
                return HttpNotFound();
            }
            return View(envios);
        }

        // POST: /Envios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Envios envios = db.Envios.Find(id);
            db.Envios.Remove(envios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
