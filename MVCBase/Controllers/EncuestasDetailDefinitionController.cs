﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class EncuestasDetailDefinitionController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /EncuestasDetailDefinition/
        public ActionResult Index()
        {
            var encuestasdetalle = db.EncuestasDetalle.Include(e => e.EncuestasMaster).Include(e => e.Prioridades).Include(e => e.Prioridades1);
            return View(encuestasdetalle.Where(x => x.activo == true).ToList());
        }

        // GET: /EncuestasDetailDefinition/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestasDetalle encuestasdetalle = db.EncuestasDetalle.Find(id);
            if (encuestasdetalle == null)
            {
                return HttpNotFound();
            }
            return View(encuestasdetalle);
        }

        // GET: /EncuestasDetailDefinition/Create
        public ActionResult Create()
        {
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster.Where(x => x.activo == true), "idEncuesta", "descripcionEncuesta");
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad");           
            return View();
        }

        // POST: /EncuestasDetailDefinition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idEncuestaDetalle,idEncuesta,idPrioridad")] EncuestasDetalle encuestasdetalle)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    encuestasdetalle.activo = true;
                    db.EncuestasDetalle.Add(encuestasdetalle);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return Message(ex);
            }
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster.Where(x => x.activo == true), "idEncuesta", "descripcionEncuesta", encuestasdetalle.idEncuesta);
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", encuestasdetalle.idPrioridad);        
            return View(encuestasdetalle);
        }

        private ActionResult Message(Exception ex)
        {
            if (ex.InnerException.HResult == -2146232060)
                ViewBag.message = "La prioridad seleccionada ya existe para dicha encuesta.";
            else ViewBag.message = ex.Message;
            ViewBag.enlace = "/EncuestasDetailDefinition/Create";
            return View("MessageView");
        }

        // GET: /EncuestasDetailDefinition/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestasDetalle encuestasdetalle = db.EncuestasDetalle.Find(id);
            if (encuestasdetalle == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster.Where(x => x.activo == true), "idEncuesta", "descripcionEncuesta", encuestasdetalle.idEncuesta);
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", encuestasdetalle.idPrioridad);
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", encuestasdetalle.idPrioridad);
            return View(encuestasdetalle);
        }

        // POST: /EncuestasDetailDefinition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idEncuestaDetalle,idEncuesta,idPrioridad")] EncuestasDetalle encuestasdetalle)
        {
            if (ModelState.IsValid)
            {
                encuestasdetalle.activo = true;
                db.Entry(encuestasdetalle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEncuesta = new SelectList(db.EncuestasMaster.Where(x => x.activo == true), "idEncuesta", "descripcionEncuesta", encuestasdetalle.idEncuesta);
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", encuestasdetalle.idPrioridad);
            ViewBag.idPrioridad = new SelectList(db.Prioridades.Where(x => x.activo == true), "idPrioridad", "descripcionPrioridad", encuestasdetalle.idPrioridad);
            return View(encuestasdetalle);
        }

        // GET: /EncuestasDetailDefinition/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EncuestasDetalle encuestasdetalle = db.EncuestasDetalle.Find(id);
            if (encuestasdetalle == null)
            {
                return HttpNotFound();
            }
            return View(encuestasdetalle);
        }

        // POST: /EncuestasDetailDefinition/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            EncuestasDetalle encuestasdetalle = db.EncuestasDetalle.Find(id);
            encuestasdetalle.activo = false;
            db.Entry(encuestasdetalle).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
