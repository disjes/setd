﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class ConfigsController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Configs/
        public ActionResult Index()
        {
            return View(db.Configs.ToList());
        }

        // GET: /Configs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configs configs = db.Configs.Find(id);
            if (configs == null)
            {
                return HttpNotFound();
            }
            return View(configs);
        }

        // GET: /Configs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Configs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="config,valor")] Configs configs)
        {
            if (ModelState.IsValid)
            {
                db.Configs.Add(configs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(configs);
        }

        // GET: /Configs/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configs configs = db.Configs.Find(id);
            if (configs == null)
            {
                return HttpNotFound();
            }
            return View(configs);
        }

        // POST: /Configs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="config,valor")] Configs configs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(configs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(configs);
        }

        // GET: /Configs/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configs configs = db.Configs.Find(id);
            if (configs == null)
            {
                return HttpNotFound();
            }
            return View(configs);
        }

        // POST: /Configs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Configs configs = db.Configs.Find(id);
            db.Configs.Remove(configs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
