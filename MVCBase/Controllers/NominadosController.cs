﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class NominadosController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /Nominados/
        public ActionResult Index()
        {
            var nominados = db.Nominados.Include(n => n.Empresas).Include(n => n.Puestos).Where(x => x.activo == true);
            return View(nominados.ToList());
        }

        // GET: /Nominados/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nominados nominados = db.Nominados.Find(id);
            if (nominados == null)
            {
                return HttpNotFound();
            }
            return View(nominados);
        }

        // GET: /Nominados/Create
        public ActionResult Create()
        {
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa");
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto");
            return View();
        }

        // POST: /Nominados/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idNominado,nombreNominado,idEmpresa,idPuesto,correo,telefono,extension,celular,interesado,activo")] Nominados nominados)
        {
            if (ModelState.IsValid)
            {
                nominados.activo = true;
                db.Nominados.Add(nominados);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominados.idEmpresa);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominados.idPuesto);
            return View(nominados);
        }

        // GET: /Nominados/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nominados nominados = db.Nominados.Find(id);
            if (nominados == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominados.idEmpresa);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominados.idPuesto);
            return View(nominados);
        }

        // POST: /Nominados/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idNominado,nombreNominado,idEmpresa,idPuesto,correo,telefono,extension,celular,interesado,activo")] Nominados nominados)
        {
            if (ModelState.IsValid)
            {
                nominados.activo = true;
                db.Entry(nominados).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominados.idEmpresa);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominados.idPuesto);
            return View(nominados);
        }

        // GET: /Nominados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nominados nominados = db.Nominados.Find(id);
            if (nominados == null)
            {
                return HttpNotFound();
            }
            return View(nominados);
        }

        // POST: /Nominados/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Nominados nominados = db.Nominados.Find(id);
            nominados.activo = false;
            db.Entry(nominados).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
