﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class NominadosDetalleController : Controller
    {
        private SETDEntities db = new SETDEntities();

        // GET: /NominadosDetalle/
        public ActionResult Index()
        {
            var nominadosdetalle = db.NominadosDetalle.Include(n => n.Empresas).Include(n => n.Nominados).Include(n => n.Puestos);
            return View(nominadosdetalle.Where(x => x.activo == true).ToList());
        }

        // GET: /NominadosDetalle/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NominadosDetalle nominadosdetalle = db.NominadosDetalle.Find(id);
            if (nominadosdetalle == null)
            {
                return HttpNotFound();
            }
            return View(nominadosdetalle);
        }

        // GET: /NominadosDetalle/Create
        public ActionResult Create()
        {
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa");
            ViewBag.idNominado = new SelectList(db.Nominados.Where(x => x.activo == true), "idNominado", "nombreNominado");
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto");
            return View();
        }

        // POST: /NominadosDetalle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idNominadosDetalle,idNominado,idEmpresa,idPuesto,anios")] NominadosDetalle nominadosdetalle)
        {
            if (ModelState.IsValid)
            {
                nominadosdetalle.activo = true;
                db.NominadosDetalle.Add(nominadosdetalle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominadosdetalle.idEmpresa);
            ViewBag.idNominado = new SelectList(db.Nominados.Where(x => x.activo == true), "idNominado", "nombreNominado", nominadosdetalle.idNominado);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominadosdetalle.idPuesto);
            return View(nominadosdetalle);
        }

        // GET: /NominadosDetalle/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NominadosDetalle nominadosdetalle = db.NominadosDetalle.Find(id);
            if (nominadosdetalle == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominadosdetalle.idEmpresa);
            ViewBag.idNominado = new SelectList(db.Nominados.Where(x => x.activo == true), "idNominado", "nombreNominado", nominadosdetalle.idNominado);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominadosdetalle.idPuesto);
            return View(nominadosdetalle);
        }

        // POST: /NominadosDetalle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idNominadosDetalle,idNominado,idEmpresa,idPuesto,anios")] NominadosDetalle nominadosdetalle)
        {
            if (ModelState.IsValid)
            {
                nominadosdetalle.activo = true;
                db.Entry(nominadosdetalle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEmpresa = new SelectList(db.Empresas.Where(x => x.activo == true), "idEmpresa", "descripcionEmpresa", nominadosdetalle.idEmpresa);
            ViewBag.idNominado = new SelectList(db.Nominados.Where(x => x.activo == true), "idNominado", "nombreNominado", nominadosdetalle.idNominado);
            ViewBag.idPuesto = new SelectList(db.Puestos.Where(x => x.activo == true), "idPuesto", "descripcionPuesto", nominadosdetalle.idPuesto);
            return View(nominadosdetalle);
        }

        // GET: /NominadosDetalle/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NominadosDetalle nominadosdetalle = db.NominadosDetalle.Find(id);
            if (nominadosdetalle == null)
            {
                return HttpNotFound();
            }
            return View(nominadosdetalle);
        }

        // POST: /NominadosDetalle/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            NominadosDetalle nominadosdetalle = db.NominadosDetalle.Find(id);
            nominadosdetalle.activo = false;
            db.Entry(nominadosdetalle).State = EntityState.Modified; 
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
