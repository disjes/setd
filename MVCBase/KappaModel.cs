﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase
{
    public class KappaModel
    {
        public int IdPregunta { get; set; }
        public int Si { get; set; }
        public int No { get; set; }
    }
}