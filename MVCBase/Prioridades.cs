//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Prioridades
    {
        public Prioridades()
        {
            this.activo = true;
            this.Conceptos = new HashSet<Conceptos>();
            this.EncuestaDetalle = new HashSet<EncuestaDetalle>();
            this.EncuestasDetalle = new HashSet<EncuestasDetalle>();
            this.EncuestasDetalle1 = new HashSet<EncuestasDetalle>();
        }
    
        public int idPrioridad { get; set; }
        public string descripcionPrioridad { get; set; }
        public Nullable<int> orden { get; set; }
        public Nullable<bool> activo { get; set; }
    
        public virtual ICollection<Conceptos> Conceptos { get; set; }
        public virtual ICollection<EncuestaDetalle> EncuestaDetalle { get; set; }
        public virtual ICollection<EncuestasDetalle> EncuestasDetalle { get; set; }
        public virtual ICollection<EncuestasDetalle> EncuestasDetalle1 { get; set; }
    }
}
