﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Excel;
using TwitterBootstrapMVC.Controls;
using WebGrease.Css.Extensions;

namespace MVCBase
{
    public static class Kappa
    {

        private static SETDEntities db = new SETDEntities();

        public static List<KappaModel> KappaOk(string user)
        {
            var encuestasRecords = db.EncuestaMaster.Where(x => x.idEncuesta == 2 && x.estado == "A").ToList();
            Application app = new Application();
            List<KappaModel> mKappaModels = new List<KappaModel>();
            db.Conceptos.SqlQuery("Select * from Conceptos where idPrioridad in(Select idPrioridad from EncuestasDetalle where idEncuesta = 2)").ForEach(item =>
            {
                mKappaModels.Add(new KappaModel()
                {
                    //Agregar filtro de los Kappas actuales.
                    IdPregunta = item.idConcepto,
                    No = db.EncuestaDetalle.Where(x => x.EncuestaMaster.idEncuesta == 2 &&
                        x.idConcepto == item.idConcepto && x.respuesta == 2).ToList().Count,
                    Si = db.EncuestaDetalle.Where(x => x.EncuestaMaster.idEncuesta == 2 &&
                        x.idConcepto == item.idConcepto && x.respuesta == 1).ToList().Count
                });                
            });
            var mKappaForCount = mKappaModels.FirstOrDefault(x => x.Si > 0 && x.No > 0);
            CalculosKappa mKappa = new CalculosKappa();
            mKappa.m = mKappaForCount.Si + mKappaForCount.No;
            mKappa.n = mKappaModels.Where(x => x.Si > 0 || x.No > 0).ToList().Count;
            mKappa.qyes = mKappaModels.Sum(x => x.Si) / (mKappa.m * mKappa.n);
            mKappa.qno = mKappaModels.Sum(x => x.No) / (mKappa.m * mKappa.n);
            mKappa.byes = mKappa.qyes * (1 - mKappa.qyes);
            mKappa.bno = mKappa.qno * (1 - mKappa.qno);
            var siArray = mKappaModels.Select(x => x.Si).ToArray();
            mKappa.kyes = 1 - Convert.ToDecimal(app.WorksheetFunction.SumProduct(mKappaModels.Select(x => x.Si).ToArray(), mKappaModels.Select(x => x.Si).ToArray())) /
                          (mKappa.m * mKappa.n * mKappa.m - 1) * mKappa.qyes * (1 - mKappa.qyes); //Pendiente
            mKappa.kno = 1 - Convert.ToDecimal(app.WorksheetFunction.SumProduct(mKappaModels.Select(x => x.No).ToArray(), mKappaModels.Select(x => x.No).ToArray())) /
                          (mKappa.m * mKappa.n * mKappa.m - 1) * mKappa.qno * (1 - mKappa.qno); //Pendiente
            mKappa.seyes = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(2 / (mKappa.m * (mKappa.m - 1) * mKappa.n))));
            mKappa.seno = mKappa.seyes;
            mKappa.zyes = mKappa.kyes / mKappa.seyes;
            mKappa.zno = mKappa.kno / mKappa.seyes;
            mKappa.pyes = Convert.ToDecimal(2 * (1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.zyes), true)));
            mKappa.pno = Convert.ToDecimal(2 * (1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.zno), true)));
            mKappa.Pa = (Convert.ToDecimal(app.WorksheetFunction.SumSq(mKappaModels.Select(x => x.Si).ToArray(),
                mKappaModels.Select(x => x.No).ToArray())) - (mKappa.m * mKappa.n)) / (mKappa.m * mKappa.n * (mKappa.m - 1));
            mKappa.Pe = Convert.ToDecimal(app.WorksheetFunction.SumSq(mKappa.qyes, mKappa.qno));
            mKappa.Kappa = (mKappa.Pa - mKappa.Pe) / (1 - mKappa.Pe);
            double sumbexp2 = app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno) *
                              app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno);
            mKappa.se = Convert.ToDecimal(Convert.ToDouble(mKappa.seyes) * Math.Sqrt(Convert.ToDouble(sumbexp2 - app.WorksheetFunction.SumProduct(mKappa.byes,
                mKappa.bno, mKappa.qyes, mKappa.qno)))) / Convert.ToDecimal(app.WorksheetFunction.Sum(mKappa.byes, mKappa.bno));
            mKappa.z = mKappa.Kappa / mKappa.se;
            mKappa.pvalue = Convert.ToDecimal(2 * (1 - app.WorksheetFunction.Norm_S_Dist(Convert.ToDouble(mKappa.z), true)));
            mKappa.alpha = Convert.ToDecimal(0.005);
            mKappa.lower = (mKappa.Kappa + mKappa.se) * Convert.ToDecimal(app.WorksheetFunction.Norm_S_Inv(Convert.ToDouble(mKappa.alpha / 2)));
            mKappa.upper = (mKappa.Kappa - mKappa.se) * Convert.ToDecimal(app.WorksheetFunction.Norm_S_Inv(Convert.ToDouble(mKappa.alpha / 2)));
            mKappa.usuario = user;
            mKappa.estado = "A";
            db.CalculosKappa.Add(mKappa);
            db.SaveChanges();
            return mKappaModels;
        }

        public static void resendSurvey(int numJudges, List<KappaModel> mKappas)
        {
            int percent50 = Convert.ToInt32(numJudges * 0.50);
            var targets = mKappas.Where(x => x.No <= (mKappas.Count - percent50) && x.No > 0);
            foreach (var target in targets)
            {
                Repeats mRepeat = new Repeats();
                var mEncuestaDetalle = db.EncuestaDetalle.FirstOrDefault(x => x.idEncuestaDetalle == target.IdPregunta);
                mRepeat.idNominado = mEncuestaDetalle.EncuestaMaster.Nominados.idNominado;
                mRepeat.idEncuestaDetalle = mEncuestaDetalle.idEncuestaDetalle;
                db.Repeats.Add(mRepeat);
                db.Envios.Add(new Envios()
                {
                    idEncuesta = 25,
                    idNominado = mRepeat.idNominado,
                    fecha = DateTime.Now,
                    state = "A"
                });
            }
            db.SaveChanges();
        }
    }
}