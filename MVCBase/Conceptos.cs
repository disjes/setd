//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Conceptos
    {
        public Conceptos()
        {
            this.activo = true;
            this.EncuestaDetalle = new HashSet<EncuestaDetalle>();
        }
    
        public int idConcepto { get; set; }
        public int idPrioridad { get; set; }
        public string descripcionConcepto { get; set; }
        public Nullable<int> orden { get; set; }
        public Nullable<bool> activo { get; set; }
    
        public virtual Prioridades Prioridades { get; set; }
        public virtual ICollection<EncuestaDetalle> EncuestaDetalle { get; set; }
    }
}
